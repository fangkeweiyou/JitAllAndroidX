package com.androidx.jetpack2019

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

/**
 * https://www.bilibili.com/medialist/detail/ml457478326 原视频列表
 * https://www.bilibili.com/video/BV1w4411t7UQ?p=23  番外篇视频列表
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}

// todo drawable-v24专用于放置矢量图的