package com.androidx.jetpack2019.star

import android.content.res.Configuration
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.androidx.jetpack2019.R
import com.androidx.resource.BaseActivity
import com.androidx.resource.dddBug
import kotlinx.android.synthetic.main.activity_star.*

/**
 * Created by zhangyuncai on 2020/8/9.
 */
class Star2Activity:BaseActivity() {
    val starViewModel by lazy {
        ViewModelProviders.of(this).get(StarViewModel::class.java)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dddBug("onCreate",javaClass)
        setContentView(R.layout.activity_star2)

        add.setOnClickListener {
            starViewModel.numLiveData.value = starViewModel.numLiveData.value!!.plus(1)
        }
        sub.setOnClickListener {
            starViewModel.numLiveData.value = starViewModel.numLiveData.value!!.minus(1)
        }
        starViewModel.numLiveData.observe(this, Observer<Int> {
            num.text = "${it}"
        })

    }
    override fun onRestart() {
        super.onRestart()
        dddBug("onRestart",javaClass)
    }
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        dddBug("onConfigurationChanged",javaClass)
    }
}