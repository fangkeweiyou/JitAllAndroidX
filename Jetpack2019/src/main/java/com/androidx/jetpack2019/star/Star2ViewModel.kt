package com.androidx.jetpack2019.star

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel

/**
 * Created by zhangyuncai on 2020/8/9.
 * todo 模拟APP被杀死保存数据,如果是用户主动杀死APP就不算
 * todo 和onSaveInstanceState 性质一样的
 * todo 获取ViewModel :  ViewModelProviders.of(this,SavedStateViewModelFactory(this.application,this)).get(StarViewModel::class.java)
 */
class Star2ViewModel(var handler: SavedStateHandle) : ViewModel() {
    val NUM = "NUM"
    fun numLiveData(): MutableLiveData<Int> {
        if (!handler.contains(NUM)) {
            handler.set(NUM, 0)
        }
        return handler.getLiveData(NUM)
    }

    fun add() {
        numLiveData().value = numLiveData().value!! + 1
    }
}