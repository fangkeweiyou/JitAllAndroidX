package com.androidx.jetpack2019.star

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.SavedStateHandle

/**
 * Created by zhangyuncai on 2020/8/9.
 */
/**
 * todo 这样既有了可以保存APP被杀死的的handler以供恢复,又有了application对象
 */
class Star3ViewModel(var handle: SavedStateHandle,application: Application) : AndroidViewModel(application) {
}