package com.androidx.jetpack2019.star

import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.androidx.jetpack2019.R
import com.androidx.jetpack2019.databinding.ActivityStarBinding
import com.androidx.resource.BaseActivity
import com.androidx.resource.dddBug
import kotlinx.android.synthetic.main.activity_star.*

/**
 * Created by zhangyuncai on 2020/8/9.
 * todo 模拟APP被杀死保存数据,如果是用户主动杀死APP就不算
 */
class StarActivity : BaseActivity() {
    val NUM="NUM"
    val starViewModel by lazy {
        ViewModelProviders.of(this).get(StarViewModel::class.java)
    }

    lateinit var activityStarBinding:ActivityStarBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        savedInstanceState?.let {
            starViewModel.numLiveData.value=it.getInt(NUM,0)
        }

         activityStarBinding =ActivityStarBinding.inflate(layoutInflater)
//            DataBindingUtil.setContentView<ActivityStarBinding>(
//                this,
//                R.layout.activity_star
//            )
        setContentView(activityStarBinding.root)
        activityStarBinding.model = starViewModel
        activityStarBinding.setLifecycleOwner(this)

        activityStarBinding.btTestviewmodel.setOnClickListener {
            val hashCode =
                ViewModelProviders.of(this).get(StarViewModel::class.java).hashCode()
            dddBug("starViewModel.hashCode:${starViewModel.hashCode()}")
            dddBug("hashCode:${hashCode}")
        }
    }

    /**
     * todo 模拟APP被杀死保存数据,如果是用户主动杀死APP就不算
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(NUM,starViewModel.numLiveData.value!!)
    }


}