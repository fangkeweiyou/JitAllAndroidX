package com.androidx.jetpack2019.star

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.androidx.jetpack2019.R
import com.androidx.jetpack2019.databinding.FragmentStarBinding
import com.androidx.resource.dddBug

/**
 * Created by zhangyuncai on 2020/8/21.
 */
class StarFragment:Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val hashCode =
            ViewModelProviders.of(requireActivity()).get(StarViewModel::class.java).hashCode()
        dddBug("fragment.hashCode:${hashCode}")
        val view = inflater.inflate(R.layout.fragment_star, container, false)
        val dataBinding = FragmentStarBinding.inflate(layoutInflater, container, false)
        dddBug("dataBinding:${dataBinding==null}")
        return  dataBinding.root
    }
}