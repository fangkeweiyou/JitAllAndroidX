package com.androidx.jetpack2019.star

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.androidx.resource.dddBug

/**
 * Created by zhangyuncai on 2020/8/9.
 */
class StarViewModel : ViewModel() {
    val numLiveData: MutableLiveData<Int> = MutableLiveData<Int>()

    init {
        numLiveData.value = 0
//        SavedStateHandle
    }
    fun add()
    {
        numLiveData.value = numLiveData.value!!.plus(1)
    }

    fun sub()
    {
        numLiveData.value = numLiveData.value!!.minus(1)
    }

    fun click(v:View)
    {
        dddBug("点击:${v.context::class.java.name}")
    }
}