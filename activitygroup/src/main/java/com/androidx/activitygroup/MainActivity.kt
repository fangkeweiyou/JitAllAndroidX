package com.androidx.activitygroup

import android.app.Activity
import android.os.Bundle

/**
 * Created by zhangyuncai on 2020/9/8.
 */
class MainActivity:Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}