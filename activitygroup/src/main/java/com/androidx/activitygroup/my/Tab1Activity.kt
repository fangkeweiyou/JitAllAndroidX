package com.androidx.activitygroup.my

import android.app.TabActivity
import android.content.Intent
import android.os.Bundle

/**
 * Created by zhangyuncai on 2020/9/8.
 */
class Tab1Activity : TabActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val tabHost = tabHost

        tabHost.addTab(
            tabHost.newTabSpec("tab1")
                .setIndicator("OPT")
                .setContent(Intent(this, Tab1ActivityA::class.java))
        )

        tabHost.addTab(
            tabHost.newTabSpec("tab2")
                .setIndicator("EDIT")
                .setContent(Intent(this, Tab1ActivityB::class.java))
        )

        tabHost.currentTab = 0
    }
}