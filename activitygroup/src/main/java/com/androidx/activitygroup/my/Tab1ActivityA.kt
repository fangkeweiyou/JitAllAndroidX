package com.androidx.activitygroup.my

import android.app.LocalActivityManager
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.androidx.activitygroup.R
import com.androidx.activitygroup.tabsample.EditActivity
import com.androidx.activitygroup.tabsample.TabGroupActivity
import kotlinx.android.synthetic.main.activity_tab1_a.*

/**
 * Created by zhangyuncai on 2020/9/8.
 */
class Tab1ActivityA:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab1_a)

        bt_tab1a.setOnClickListener {
            val tab1Activity = parent as Tab1Activity
//            tab1Activity.startActivity(Intent(Tab1ActivityA@this,Tab1ActivityC::class.java))
            val localActivityManager = tab1Activity.localActivityManager
            val startActivity = localActivityManager.startActivity(
                "Tab1ActivityC",
                Intent(Tab1ActivityA@ this, Tab1ActivityC::class.java)
            )
        }
    }
}