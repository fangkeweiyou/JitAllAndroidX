package com.androidx.activitygroup.my

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.androidx.activitygroup.R
import kotlinx.android.synthetic.main.activity_tab1_b.*

/**
 * Created by zhangyuncai on 2020/9/8.
 */
class Tab1ActivityB:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab1_b)

        bt_tab1b.setOnClickListener {
            startActivity(Intent(Tab1ActivityB@this,Tab1ActivityC::class.java))
        }
    }
}