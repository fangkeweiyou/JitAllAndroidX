package com.androidx.android.banner

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.androidx.android.R
import com.bumptech.glide.Glide
import com.youth.banner.adapter.BannerAdapter

/**
 * Created by zhangyuncai on 2020/8/15.
 */
class BannerAdapter(list: MutableList<String>) :
    BannerAdapter<String, com.androidx.android.banner.BannerAdapter.MyViewHolder2>(list) {
    override fun onCreateHolder(parent: ViewGroup, viewType: Int): MyViewHolder2 {
        return MyViewHolder2(
            LayoutInflater.from(parent.context).inflate(R.layout.item_banner_adapter, parent, false)
        )
    }

    override fun onBindView(holder: MyViewHolder2, data: String, position: Int, size: Int) {
        Glide.with(holder.imageView.context)
            .load(data)
            .into(holder.imageView)
    }

    inner class MyViewHolder2(val view: View) : RecyclerView.ViewHolder(view) {
        val imageView: ImageView = view.findViewById(R.id.iv_banner)
    }

}