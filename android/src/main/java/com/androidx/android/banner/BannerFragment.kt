package com.androidx.android.banner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidx.android.R
import com.androidx.resource.BaseFragment
import com.androidx.resource.showToast
import com.youth.banner.indicator.CircleIndicator
import kotlinx.android.synthetic.main.fragment_banner.*

/**
 * Created by zhangyuncai on 2020/8/15.
 */
class BannerFragment:BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_banner,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val list= mutableListOf<String>()
        for(index in 0..10)
        {
            list.add("http://www.fangkeweiyou.com/meinv/$index")
        }
        val bannerAdapter = BannerAdapter(list)
        banner.adapter=bannerAdapter
        banner.addBannerLifecycleObserver(this)
        banner.setIndicator(CircleIndicator(requireContext()))
        banner.setOnBannerListener { data, position ->
            showToast("$position")
        }
    }
}