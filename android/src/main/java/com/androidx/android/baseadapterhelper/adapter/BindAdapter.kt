package com.androidx.android.baseadapterhelper.adapter

import com.androidx.android.R
import com.androidx.android.databinding.ItemAdapterbindAdapterBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder

/**
 * Created by zhangyuncai on 2020/8/15.
 */
class BindAdapter(list: MutableList<String>) :
    BaseQuickAdapter<String, BaseDataBindingHolder<ItemAdapterbindAdapterBinding>>(
        R.layout.item_adapterbind_adapter, list
    ) {
    override fun convert(
        holder: BaseDataBindingHolder<ItemAdapterbindAdapterBinding>,
        item: String
    ) {
        holder.dataBinding!!.item = item
    }
}