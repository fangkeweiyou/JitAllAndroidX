package com.androidx.android.baseadapterhelper.adapter

import com.androidx.android.R
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

/**
 * Created by zhangyuncai on 2020/8/15.
 */
class CommonAdapter(list:MutableList<String>):
BaseQuickAdapter<String,BaseViewHolder>(R.layout.item_adaptercommon_adapter,list){
    override fun convert(holder: BaseViewHolder, item: String) {
        holder.setText(R.id.tv_adaptercommon_content,item)
    }
}