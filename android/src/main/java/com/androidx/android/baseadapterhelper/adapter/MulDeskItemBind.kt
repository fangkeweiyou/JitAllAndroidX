package com.androidx.android.baseadapterhelper.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.androidx.android.baseadapterhelper.model.MulDeskModel
import com.androidx.android.databinding.ItemAdaptermuldeskAdapterBinding
import com.androidx.resource.dddBug
import com.chad.library.adapter.base.binder.QuickDataBindingItemBinder

/**
 * Created by zhangyuncai on 2020/8/15.
 */
class MulDeskItemBind:QuickDataBindingItemBinder<MulDeskModel,ItemAdaptermuldeskAdapterBinding>() {
    override fun convert(
        holder: BinderDataBindingHolder<ItemAdaptermuldeskAdapterBinding>,
        data2: MulDeskModel
    ) {
        dddBug(data.size)
        holder.dataBinding.item=data2
    }

    override fun onCreateDataBinding(
        layoutInflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): ItemAdaptermuldeskAdapterBinding {
        return ItemAdaptermuldeskAdapterBinding.inflate(layoutInflater,parent,false)
    }
}