package com.androidx.android.baseadapterhelper.adapter

import com.androidx.android.R
import com.androidx.android.baseadapterhelper.model.MulDisplayModel
import com.chad.library.adapter.base.binder.QuickItemBinder
import com.chad.library.adapter.base.viewholder.BaseViewHolder

/**
 * Created by zhangyuncai on 2020/8/15.
 */
class MulDislplayItemBind2 : QuickItemBinder<MulDisplayModel>() {
    override fun convert(holder: BaseViewHolder, data: MulDisplayModel) {
        holder.setText(R.id.tv_adaptercommon_content, data.name)
    }

    override fun getLayoutId(): Int {
        return R.layout.item_adaptercommon_adapter
    }
}