package com.androidx.android.baseadapterhelper.fragment

import android.os.Bundle
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidx.android.R
import com.androidx.android.baseadapterhelper.adapter.CommonAdapter
import com.androidx.resource.BaseFragment
import com.chad.library.adapter.base.BaseQuickAdapter
import kotlinx.android.synthetic.main.fragment_adapteranim.*

/**
 * Created by zhangyuncai on 2020/8/15.
 */
class AdapterAnimFragment : BaseFragment() {
    lateinit var commonAdapter: CommonAdapter
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_adapteranim, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val list = mutableListOf<String>()
        for (index in 0..100) {
            list.add("index:$index")
        }
        rv_adapteranim.layoutManager = LinearLayoutManager(requireContext())
        commonAdapter = CommonAdapter(list)
        setAnimType(BaseQuickAdapter.AnimationType.AlphaIn)

        types.text = SpannableStringBuilder()
            .append(SpannableString("AlphaIn").apply {
                setSpan(object : ClickableSpan() {
                    override fun onClick(p0: View) {
                        setAnimType(BaseQuickAdapter.AnimationType.AlphaIn)
                    }

                }, 0, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            })
            .append(SpannableString("AlphaIn").apply {
                setSpan(object : ClickableSpan() {
                    override fun onClick(p0: View) {
                        setAnimType(BaseQuickAdapter.AnimationType.AlphaIn)
                    }

                }, 0, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            })
            .append(SpannableString("ScaleIn").apply {
                setSpan(object : ClickableSpan() {
                    override fun onClick(p0: View) {
                        setAnimType(BaseQuickAdapter.AnimationType.ScaleIn)
                    }

                }, 0, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            })
            .append(SpannableString("SlideInBottom").apply {
                setSpan(object : ClickableSpan() {
                    override fun onClick(p0: View) {
                        setAnimType(BaseQuickAdapter.AnimationType.SlideInBottom)
                    }

                }, 0, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            })
            .append(SpannableString("SlideInLeft").apply {
                setSpan(object : ClickableSpan() {
                    override fun onClick(p0: View) {
                        setAnimType(BaseQuickAdapter.AnimationType.SlideInLeft)
                    }

                }, 0, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            })
            .append(SpannableString("SlideInRight").apply {
                setSpan(object : ClickableSpan() {
                    override fun onClick(p0: View) {
                        setAnimType(BaseQuickAdapter.AnimationType.SlideInRight)
                    }

                }, 0, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            })
        types.movementMethod = LinkMovementMethod.getInstance()
    }

    /**
     * 设置动画效果
     */
    private fun setAnimType(type: BaseQuickAdapter.AnimationType) {
        commonAdapter.setAnimationWithDefault(type)
        rv_adapteranim.adapter = commonAdapter
    }
}