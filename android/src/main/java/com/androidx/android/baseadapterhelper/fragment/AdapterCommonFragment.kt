package com.androidx.android.baseadapterhelper.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidx.android.R
import com.androidx.android.baseadapterhelper.adapter.CommonAdapter
import com.androidx.resource.BaseFragment
import kotlinx.android.synthetic.main.fragment_adaptercommon.*

/**
 * Created by zhangyuncai on 2020/8/15.
 */
class AdapterCommonFragment:BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_adaptercommon,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val list= mutableListOf<String>()
        for(index in 0 ..100)
        {
            list.add("index:$index")
        }
        val commonAdapter = CommonAdapter(list)
        rv_adaptercommon.adapter=commonAdapter
    }


}