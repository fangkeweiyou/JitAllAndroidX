package com.androidx.android.baseadapterhelper.fragment

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.androidx.android.R
import com.androidx.android.baseadapterhelper.adapter.CommonAdapter
import com.androidx.resource.BaseFragment
import kotlinx.android.synthetic.main.fragment_adaptercommon.*

/**
 * Created by zhangyuncai on 2020/8/15.
 */
class AdapterEmptyFragment:BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_adaptercommon,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val commonAdapter = CommonAdapter(arrayListOf())
        val emptyView=TextView(requireContext())
        val layoutParams=RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT,RecyclerView.LayoutParams.MATCH_PARENT)
        emptyView.layoutParams=layoutParams
        emptyView.text="empty"
        emptyView.gravity=Gravity.CENTER
        commonAdapter.setEmptyView(emptyView)
        rv_adaptercommon.adapter=commonAdapter
    }
}