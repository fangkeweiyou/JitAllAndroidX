package com.androidx.android.baseadapterhelper.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidx.android.R
import com.androidx.android.baseadapterhelper.adapter.MulDeskItemBind
import com.androidx.android.baseadapterhelper.adapter.MulDislplayItemBind
import com.androidx.android.baseadapterhelper.adapter.MulDislplayItemBind2
import com.androidx.android.baseadapterhelper.model.MulDeskModel
import com.androidx.android.baseadapterhelper.model.MulDisplayModel
import com.androidx.resource.BaseFragment
import com.chad.library.adapter.base.BaseBinderAdapter
import kotlinx.android.synthetic.main.fragment_adaptercommon.*

/**
 * Created by zhangyuncai on 2020/8/15.
 */
class AdapterMulBindFragment : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_adaptercommon, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val list = mutableListOf<Any>()
        for (index in 0..100) {
            if (index % 2 == 0) {
                list.add(MulDisplayModel().apply {
                    name = "$index,显示器"
                })
            } else {
                list.add(MulDeskModel().apply {
                    name = "$index,桌面"
                })
            }
        }
        val adapter = BaseBinderAdapter()

        adapter.addItemBinder(MulDisplayModel::class.java, MulDislplayItemBind())
        adapter.addItemBinder(MulDisplayModel::class.java, MulDislplayItemBind2())
//        adapter.addItemBinder(MulDisplayModel::class.java, MulDislplayItemBind())
        adapter.addItemBinder(MulDeskModel::class.java, MulDeskItemBind())


        adapter.setList(list)
        rv_adaptercommon.adapter = adapter
    }

}