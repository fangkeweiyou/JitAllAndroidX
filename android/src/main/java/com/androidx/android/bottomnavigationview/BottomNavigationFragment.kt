package com.androidx.android.bottomnavigationview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidx.android.R
import com.androidx.resource.BaseFragment
import com.androidx.resource.showToast
import kotlinx.android.synthetic.main.fragment_bottomnavigation.*

/**
 * Created by zhangyuncai on 2020/8/18.
 */
class BottomNavigationFragment:BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_bottomnavigation,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bnv.setOnNavigationItemSelectedListener {
            showToast("${it.title}")
            true
        }
    }
}