package com.androidx.jetpack2019.constraint

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidx.android.R

import com.androidx.resource.BaseFragment
import kotlinx.android.synthetic.main.fragment_chronometer.*

/**
 * Created by zhangyuncai on 2020/8/18.
 */
class ChronometerFragment:BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_chronometer,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycle.addObserver(chronometer)
    }
}