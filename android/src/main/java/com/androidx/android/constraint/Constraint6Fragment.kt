package com.androidx.jetpack2019.constraint

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidx.android.R

import com.androidx.resource.BaseFragment

/**
 * Created by zhangyuncai on 2020/8/18.
 */
class Constraint6Fragment:BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_constraint6,container,false)
    }
}