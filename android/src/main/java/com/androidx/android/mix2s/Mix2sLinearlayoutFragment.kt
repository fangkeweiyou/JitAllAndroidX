package com.androidx.android.mix2s

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.androidx.android.R

/**
 * Created by zhangyuncai on 2020/9/2.
 */
class Mix2sLinearlayoutFragment:Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_mix2s_linearlayout,container,false)
    }
}