package com.androidx.android.motionlayout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidx.android.R
import com.androidx.resource.BaseFragment

/**
 * Created by zhangyuncai on 2020/8/18.
 */
class MotionlayoutFragment:BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_motionlayout,container,false)
    }
}