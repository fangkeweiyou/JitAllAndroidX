package com.androidx.android.rxbinding

import android.view.View
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import java.util.concurrent.TimeUnit

/**
 * Created by zhangyuncai on 2020/9/4.
 */

fun View.lastClick(callback: Consumer<View>, composite: CompositeDisposable? = null) {
    var disposable: Disposable? = null
    setOnClickListener {
        disposable?.dispose()
        disposable = Observable.timer(300, TimeUnit.MILLISECONDS)
            .map { this }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(callback, Consumer { it.printStackTrace() })
        composite?.add(disposable!!)
    }
}