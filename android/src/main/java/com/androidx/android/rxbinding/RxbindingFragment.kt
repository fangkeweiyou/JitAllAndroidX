package com.androidx.android.rxbinding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.androidx.android.R
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.fragment_rxbinding.*

/**
 * Created by zhangyuncai on 2020/9/4.
 */
class RxbindingFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_rxbinding, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bt_add.lastClick(Consumer {
            add()
        })
    }

    private var index = 0
    private fun add() {
        tv_content.text = "${index++}"
    }

    private fun sub() {
        tv_content.text = "${index--}"
    }
}