package com.androidx.android.sharepreference

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidx.android.R
import com.androidx.resource.BaseFragment
import com.androidx.resource.showToast
import kotlinx.android.synthetic.main.fragment_sharepreference.*

/**
 * Created by zhangyuncai on 2020/8/18.
 */
class SharepreferenceFragment:BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sharepreference,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        save.setOnClickListener {
            showToast("已保存")
            requireContext().getSharedPreferences("sharepre1", Context.MODE_PRIVATE)
                .edit()
                .putString("name", "zhangsan")
                .apply()
        }
        save2.setOnClickListener {
            showToast("已保存")
            requireContext().getSharedPreferences("sharepre2", Context.MODE_PRIVATE)
                .edit()
                .putString("name2", "lisi")
                .apply()
        }
    }
}