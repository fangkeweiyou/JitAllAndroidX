package com.androidx.android.smartrefreshlayout

import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidx.android.R
import com.androidx.resource.BaseFragment
import com.scwang.smart.refresh.header.MaterialHeader
import kotlinx.android.synthetic.main.fragment_smartrefreshlayout.*
import kotlin.concurrent.thread

/**
 * Created by zhangyuncai on 2020/8/21.
 */
class SmartFragment:BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_smartrefreshlayout,container,false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        auto_smart.setRefreshHeader(MaterialHeader(requireContext()))
        auto_smart.setOnRefreshListener {
            thread {
                SystemClock.sleep(1000)
                requireActivity().runOnUiThread {
                    auto_smart.finishRefresh()
                }
            }
        }
        auto_smart.setEnableLoadMore(true)
        auto_smart.setOnLoadMoreListener {
            SystemClock.sleep(1000)
            requireActivity().runOnUiThread {
//                auto_smart.finishLoadMore()
                auto_smart.finishLoadMoreWithNoMoreData()
            }
        }
    }
}