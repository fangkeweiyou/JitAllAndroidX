package com.androidx.android.toolbar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.androidx.android.R
import com.androidx.resource.BaseFragment
import com.androidx.resource.showToast
import kotlinx.android.synthetic.main.fragment_tool1.*

/**
 * Created by zhangyuncai on 2020/8/18.
 */
class Tool1Fragment : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tool1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setBackColor()
    }



    //设置背景色
    //app图标，主标题，子标题，标题，搜索按钮，自添加按钮
    fun setBackColor() {
        (requireActivity() as AppCompatActivity).setSupportActionBar(toolbar)
        toolbar.setBackgroundColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.common_text_black
            )
        )
        toolbar.setTitle("title")
        toolbar.setLogoDescription("setLogoDescription")
        toolbar.setSubtitle("subtitle")
        toolbar.setTitleTextColor(ContextCompat.getColor(requireContext(), R.color.green))
        toolbar.setSubtitleTextColor(ContextCompat.getColor(requireContext(), R.color.red))
        //返回按钮
        toolbar.setNavigationIcon(R.drawable.menu_share)
//        toolbar.setNavigationContentDescription(R.string.app_name)//这个会替换掉title属性
        toolbar.setNavigationContentDescription("返回内容")//这个会替换掉title属性
        toolbar.setNavigationOnClickListener {
            showToast("NavigationIcon")
        }
    }
}