package com.androidx.android.viewpager2.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.androidx.android.R
import com.androidx.resource.BaseFragment
import com.androidx.resource.dddBug
import kotlinx.android.synthetic.main.fragment_viewpager2_views.*

/**
 * Created by zhangyuncai on 2020/8/17.
 */
class ViewPager2Fragment : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_viewpager2_views, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = object : FragmentStateAdapter(this) {
            override fun getItemCount(): Int {
                return 10
            }

            override fun createFragment(position: Int): Fragment {
                return ChildFragment()
            }

        }
        vp2_viewpager.adapter=adapter
        vp2_viewpager.offscreenPageLimit=10
    }

     class ChildFragment : BaseFragment() {
        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            dddBug("ChildFragment")
            return inflater.inflate(R.layout.fragment_child, container, false)
        }
    }
}