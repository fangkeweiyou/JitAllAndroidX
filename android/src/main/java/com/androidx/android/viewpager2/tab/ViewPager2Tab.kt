package com.androidx.android.viewpager2.tab

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import com.androidx.android.R
import com.androidx.android.viewpager2.views.ViewsAdapter
import com.androidx.resource.BaseFragment
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_viewpager2_tab.*

/**
 * Created by zhangyuncai on 2020/8/17.
 */
class ViewPager2Tab : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_viewpager2_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val list = mutableListOf<String>()
        for (index in 0..10) {
            list.add("index:$index")
        }
        vp2_viewpager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        val viewsAdapter = ViewsAdapter(list)
        vp2_viewpager.adapter = viewsAdapter

        val mAnimator = ViewPager2.PageTransformer { page, position ->
            val absPos = Math.abs(position)
            page.apply {
                val scale = if (absPos > 1) 0F else 1 - absPos
                scaleX = scale
                scaleY = scale
            }
        }

        vp2_viewpager.setPageTransformer(mAnimator)

        TabLayoutMediator(tab_tabs, vp2_viewpager) { tab, position ->
            tab.text="position:$position"
        }.attach()

        fab_tabs.setOnClickListener {
            list.removeAt(0)
            viewsAdapter.notifyItemRemoved(0)
//            viewsAdapter.notifyDataSetChanged()
        }
    }
}