package com.androidx.android.viewpager2.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.viewpager2.widget.ViewPager2
import com.androidx.android.R
import com.androidx.resource.BaseFragment
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import kotlinx.android.synthetic.main.fragment_viewpager2_views.*

/**
 * Created by zhangyuncai on 2020/8/17.
 */
class ViewPager2ViewsFragment : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_viewpager2_views, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val list = mutableListOf<String>()
        for (index in 0..100) {
            list.add("index:$index")
        }
        vp2_viewpager.orientation= ViewPager2.ORIENTATION_HORIZONTAL
        vp2_viewpager.adapter = ViewsAdapter(list)

        val mAnimator=ViewPager2.PageTransformer { page, position ->
            val absPos = Math.abs(position)
            page.apply {
                val scale = if (absPos > 1) 0F else 1 - absPos
                scaleX = scale
                scaleY = scale
            }
        }

        vp2_viewpager.setPageTransformer(mAnimator)
    }
}

class ViewsAdapter(list: MutableList<String>) :
    BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_viewpager2_views_adapter, list) {
    override fun convert(helper: BaseViewHolder, item: String) {
        helper.setText(R.id.tv_views_content, item)

        if (helper.layoutPosition % 2 == 0) {
            helper.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.red))
        } else {
            helper.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.green))
        }
    }

}