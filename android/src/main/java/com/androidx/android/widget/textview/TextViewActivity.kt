package com.androidx.android.widget.textview

import android.os.Bundle
import android.view.View
import android.widget.Button
import com.androidx.android.R
import com.androidx.resource.BaseActivity
import kotlinx.android.synthetic.main.activity_textview.*

/**
 * Created by zhangyuncai on 2020/8/4.
 */
class TextViewActivity:BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_textview)

//        ll_contanier.removeAllViews()

        addButton("textview",View.OnClickListener {

        })
    }


    private fun addButton(content:String,onClickListener: View.OnClickListener)
    {
        val button=Button(mActivity)
        button.text=content
        button.setOnClickListener(onClickListener)
        ll_contanier.addView(button)
    }
}