package com.androidx.android.获取主线程的方式

import android.app.Activity
import android.os.AsyncTask
import android.os.Handler
import android.os.Looper
import android.view.View

/**
 * Created by zhangyuncai on 2020/8/6.
 * 获取主线程的方式
 */
fun getMainThread1(view: View, runnable: Runnable) {
    view.post(runnable)
}

fun getMainThread2(activity: Activity, runnable: Runnable) {
    activity.runOnUiThread(runnable)
}

fun getMainThread3(runnable: Runnable) {
    Handler(Looper.getMainLooper()!!).post(runnable)
}

fun getMainThread4()
{
//    AsyncTask
}

fun getMainThread5()
{
    //RxJava
}