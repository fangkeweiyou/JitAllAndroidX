package com.androidx.jitallandroidx

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        https://developer.android.com/jetpack/androidx?hl=zh-cn
//        https://developer.android.com/reference/androidx/packages?hl=zh-cn
    }
}