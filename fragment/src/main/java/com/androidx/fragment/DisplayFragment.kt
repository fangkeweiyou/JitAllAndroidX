package com.androidx.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.androidx.resource.dddBug
import kotlinx.android.synthetic.main.fragment_display.*

/**
 * Created by zhangyuncai on 2020/8/19.
 */
class DisplayFragment(var index:Int) : Fragment() {
    override fun onAttach(context: Context) {
        super.onAttach(context)
        dddBug("onAttach${index}",this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dddBug("onCreate${index}",this)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dddBug("onCreateView${index}",this)
        return inflater.inflate(R.layout.fragment_display, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bt_displayfragment.text="${index}哈哈"
        dddBug("onViewCreated${index}",this)
    }

    override fun onResume() {
        super.onResume()
        dddBug("onResume${index}",this)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        dddBug("onHiddenChange${index}",this)
    }

    override fun onStart() {
        super.onStart()
        dddBug("onStart${index}",this)
    }

}