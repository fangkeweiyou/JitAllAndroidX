package com.androidx.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.androidx.fragment.bottomfragment.BottomfragmentActivity
import com.androidx.fragment.framelayoutfragment.FramelayoutFragmentActivity
import com.androidx.fragment.navigationfragment.NavigationFragmentActivity
import com.androidx.fragment.viewpager2fragment.Viewpager2FragmentActivity
import com.androidx.fragment.viewpagerfragment.ViewpagerFragmentActivity
import kotlinx.android.synthetic.main.activity_fragmentlist.*

/**
 * Created by zhangyuncai on 2020/9/1.
 */
class FragmentListActivity : AppCompatActivity() {
    val mActivity by lazy { this }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragmentlist)

        ll_fragmentlist_container.removeAllViews()
        addButton("FramelayoutFragment的生命周期", View.OnClickListener {
            startActivity(Intent(mActivity,
                FramelayoutFragmentActivity::class.java))
        })
        addButton("NavigationFragment的生命周期", View.OnClickListener {
            startActivity(Intent(mActivity,
                NavigationFragmentActivity::class.java))
        })
        addButton("ViewpagerFragment的生命周期", View.OnClickListener {
            startActivity(Intent(mActivity,
                ViewpagerFragmentActivity::class.java))
        })
        addButton("Viewpager2Fragment的生命周期", View.OnClickListener {
            startActivity(Intent(mActivity,
                Viewpager2FragmentActivity::class.java))
        })
        addButton("Bottomfragment的生命周期", View.OnClickListener {
            startActivity(Intent(mActivity,
                BottomfragmentActivity::class.java))
        })

    }

    private fun addButton(content: String, onClickListener: View.OnClickListener) {
        val button = Button(this)
        button.text=content
        ll_fragmentlist_container.addView(button)
        button.setOnClickListener(onClickListener)
    }
}