package com.androidx.fragment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragment1 = DisplayFragment(1)
        val fragment2 = DisplayFragment(2)
        val fragment3 = DisplayFragment(3)
        val fragment4 = DisplayFragment(4)

        bt_fragment_add.setOnClickListener {
        }
        var index = 0
        bt_fragment_hide.setOnClickListener {
            index++
            if (index > 4) {
                index = 0
            }
            var fragment: DisplayFragment
            val beginTransaction = supportFragmentManager.beginTransaction()
            beginTransaction.hide(fragment1)
            beginTransaction.hide(fragment2)
            beginTransaction.hide(fragment3)
            beginTransaction.hide(fragment4)
            when (index) {
                0 -> {
                    fragment = fragment1
                }
                1 -> {
                    fragment = fragment2
                }
                2 -> {
                    fragment = fragment3
                }
                else -> {
                    fragment = fragment4
                }
            }
            beginTransaction.show(fragment)
                .commit()
        }
        supportFragmentManager.beginTransaction()
            .add(R.id.cons_fragment_main, fragment1)
            .add(R.id.cons_fragment_main, fragment2)
            .add(R.id.cons_fragment_main, fragment3)
            .add(R.id.cons_fragment_main, fragment4)
            .show(fragment1)
            .commit()
    }
}