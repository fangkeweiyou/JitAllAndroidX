package com.androidx.fragment.bottomfragment

import android.app.ActivityGroup
import android.content.Context
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.androidx.fragment.R
import com.androidx.resource.dddBug
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.activity_bottomfragment.*

/**
 * Created by zhangyuncai on 2020/9/2.
 */
class BottomfragmentActivity : AppCompatActivity() {
    private val content = StringBuffer()
    private val bottomChildBottomFragment by lazy {
        BottomChildBottomFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottomfragment)

        bt_bottomfragment_show.setOnClickListener {
            bottomChildBottomFragment.show(supportFragmentManager, "BottomChildBottomFragment")
        }

        tv_bottomfragment_log.movementMethod = ScrollingMovementMethod()
    }

    fun addText(text: String) {
        content.append("${text}\n")
        tv_bottomfragment_log.text = content.toString()
        dddBug(text)
    }
}

/*
//todo show
init
onAttach
onCreate
onCreateView
onViewCreated
onActivityCreated
onViewStateRestored
onStart
onResume

//todo dismiss
onPause
onStop
onDestroyView
onDestroy
onDetach
 */
class BottomChildBottomFragment : BottomSheetDialogFragment() {
    val mActivity by lazy { requireActivity() as BottomfragmentActivity }
    lateinit var rootView: View
    private fun addText(text: String) {
        mActivity.addText(text)
    }

    init {
        dddBug("init")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        addText("onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addText("onCreate")
    }

    var index = 0
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (this::rootView.isInitialized) {
            if (rootView.parent != null && rootView.parent!! is ViewGroup) {
                (rootView.parent as ViewGroup).removeAllViews()
            }
            return rootView
        }
        val button = Button(requireContext())
        button.text = "BottomChildBottomFragment"
        button.setOnClickListener {
            button.text = "BottomChildBottomFragment:${index++}"
        }
        addText("onCreateView")
        rootView = button
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addText("onViewCreated")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        addText("onActivityCreated")
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        addText("onViewStateRestored")
    }

    override fun onStart() {
        super.onStart()
        addText("onStart")
    }

    override fun onResume() {
        super.onResume()
        addText("onResume")
    }

    override fun onPause() {
        super.onPause()
        addText("onPause")
    }

    override fun onStop() {
        super.onStop()
        addText("onStop")
    }


    override fun onDestroyView() {
        super.onDestroyView()
        addText("onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        addText("onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        addText("onDetach")
    }


    /**
     * todo show/hide管用
     */
    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        addText("onHiddenChanged")
    }

    /**
     * todo androidx没有这个生命周期
     */
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        addText("setUserVisibleHint")
    }

    /**
     * todo 不知作用
     */
    override fun onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu()
        addText("onDestroyOptionsMenu")
    }

    /**
     * todo 不知作用
     */
    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        addText("onAttachFragment")
    }

    /**
     * todo 不知作用
     */
    override fun onInflate(context: Context, attrs: AttributeSet, savedInstanceState: Bundle?) {
        super.onInflate(context, attrs, savedInstanceState)
        addText("onInflate")
    }

}