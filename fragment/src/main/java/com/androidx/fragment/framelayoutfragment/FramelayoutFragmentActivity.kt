package com.androidx.fragment.framelayoutfragment

import android.content.Context
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.androidx.fragment.R
import com.androidx.resource.dddBug
import kotlinx.android.synthetic.main.activity_framelayoutfragment.*

/**
 * Created by zhangyuncai on 2020/9/1.
 */
class FramelayoutFragmentActivity : AppCompatActivity() {
    private val content = StringBuffer()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_framelayoutfragment)
        val commentFragment =
            FramelayoutChildFragment()
        supportFragmentManager.beginTransaction()

        bt_framelayoutfragment_add.setOnClickListener {
            supportFragmentManager.beginTransaction()
                .add(R.id.fl_framelayoutfragment_container, commentFragment)
                .commit()
        }
        bt_framelayoutfragment_show.setOnClickListener {
            supportFragmentManager.beginTransaction()
                .show(commentFragment)
                .commit()
        }
        bt_framelayoutfragment_hide.setOnClickListener {
            supportFragmentManager.beginTransaction()
                .hide(commentFragment)
                .commit()
        }
        bt_framelayoutfragment_remove.setOnClickListener {
            supportFragmentManager.beginTransaction()
                .remove(commentFragment)
                .commit()
        }
        tv_framelayoutfragment_log.setMovementMethod(ScrollingMovementMethod())
    }


    fun addText(text: String) {
        content.append("${text}\n")
        tv_framelayoutfragment_log.text = content.toString()
        dddBug(text)
    }
}

/*
//todo add操作
onAttach
onCreate
onCreateView
onViewCreated
onActivityCreated
onViewStateRestored
onStart
onResume

//todo show/hide操作
onHiddenChanged

//todo 进入其他页面返回操作
onPause
onStop
onStart
onResume

//todo remove操作
onPause
onStop
onDestroyView
onDestroy
onDetach

 */

class FramelayoutChildFragment : Fragment() {
    val mActivity by lazy { requireActivity() as FramelayoutFragmentActivity }
    private fun addText(text: String) {
        mActivity.addText(text)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        addText("onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addText("onCreate")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val button = Button(requireContext())
        button.text = "CommentFragment"
        addText("onCreateView")
        return button
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addText("onViewCreated")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        addText("onActivityCreated")
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        addText("onViewStateRestored")
    }

    override fun onStart() {
        super.onStart()
        addText("onStart")
    }

    override fun onResume() {
        super.onResume()
        addText("onResume")
    }

    override fun onPause() {
        super.onPause()
        addText("onPause")
    }

    override fun onStop() {
        super.onStop()
        addText("onStop")
    }


    override fun onDestroyView() {
        super.onDestroyView()
        addText("onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        addText("onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        addText("onDetach")
    }


    /**
     * todo show/hide管用
     */
    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        addText("onHiddenChanged")
    }

    /**
     * todo androidx没有这个生命周期
     */
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        addText("setUserVisibleHint")
    }

    /**
     * todo 不知作用
     */
    override fun onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu()
        addText("onDestroyOptionsMenu")
    }

    /**
     * todo 不知作用
     */
    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        addText("onAttachFragment")
    }

    /**
     * todo 不知作用
     */
    override fun onInflate(context: Context, attrs: AttributeSet, savedInstanceState: Bundle?) {
        super.onInflate(context, attrs, savedInstanceState)
        addText("onInflate")
    }

}
