package com.androidx.fragment.navigationfragment

import android.content.Context
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.androidx.fragment.R
import com.androidx.resource.dddBug
import kotlinx.android.synthetic.main.activity_framelayoutfragment.*
import kotlinx.android.synthetic.main.activity_navigationfragment.*

/**
 * Created by zhangyuncai on 2020/9/1.
 */
class NavigationFragmentActivity : AppCompatActivity() {
    private val content = StringBuffer()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigationfragment)

        val findNavController = findNavController(R.id.nav_navigationfragment)
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_navigationfragment) as NavHostFragment
        val startArgs = Bundle().apply { putInt("index", 1) }
//        navHostFragment.arguments = startArgs
        navHostFragment.navController.setGraph(R.navigation.nav_navigationfragment, startArgs)
        tv_navigationfragment_log.movementMethod = ScrollingMovementMethod()
    }

    fun addText(text: String) {
        content.append("${text}\n")
        tv_navigationfragment_log.text = content.toString()
        dddBug(text)
    }
}

/*
//todo 首页
1,onAttach
1,onCreate
1,onCreateView
1,onViewCreated
1,onActivityCreated
1,onViewStateRestored
1,onStart
1,onResume

//todo 进入其他页面返回操作
1,onPause
1,onStop
1,onStart
1,onResume

//todo 进入第二个页面
2,onAttach
2,onCreate
2,onCreateView
2,onViewCreated
2,onActivityCreated
2,onViewStateRestored
2,onStart
2,onResume
1,onPause
1,onStop
1,onDestroyView

//todo 从第二个页面返回本页面
1,onCreateView
1,onViewCreated
1,onActivityCreated
1,onViewStateRestored
1,onStart
1,onResume
2,onPause
2,onStop
2,onDestroyView
2,onDestroy
2,onDetach
 */
class NavigationChildFragment : Fragment() {
    var index: Int = 0
    val mActivity by lazy { requireActivity() as NavigationFragmentActivity }
    private fun addText(text: String) {
        mActivity.addText("index:$index,$text")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.let {
            index = it.getInt("index", 0)
        }
        addText("onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addText("onCreate")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val button = Button(requireContext())

        addText("onCreateView")

        button.text = "NavigationChildFragment:${index}"
        if (index == 1) {
            button.setOnClickListener {
                val startArgs = Bundle().apply { putInt("index", 2) }
                findNavController().navigate(R.id.action_navigationChildFragment_to_navigationChildFragment2,startArgs)
            }
        }
        return button
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addText("onViewCreated")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        addText("onActivityCreated")
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        addText("onViewStateRestored")
    }

    override fun onStart() {
        super.onStart()
        addText("onStart")
    }

    override fun onResume() {
        super.onResume()
        addText("onResume")
    }

    override fun onPause() {
        super.onPause()
        addText("onPause")
    }

    override fun onStop() {
        super.onStop()
        addText("onStop")
    }


    override fun onDestroyView() {
        super.onDestroyView()
        addText("onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        addText("onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        addText("onDetach")
    }


    /**
     * todo show/hide管用
     */
    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        addText("onHiddenChanged")
    }

    /**
     * todo androidx没有这个生命周期
     */
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        addText("setUserVisibleHint")
    }

    /**
     * todo 不知作用
     */
    override fun onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu()
        addText("onDestroyOptionsMenu")
    }

    /**
     * todo 不知作用
     */
    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        addText("onAttachFragment")
    }

    /**
     * todo 不知作用
     */
    override fun onInflate(context: Context, attrs: AttributeSet, savedInstanceState: Bundle?) {
        super.onInflate(context, attrs, savedInstanceState)
        addText("onInflate")
    }

}
