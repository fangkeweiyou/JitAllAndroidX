package com.androidx.fragment.viewpager2fragment

import android.content.Context
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.androidx.fragment.R
import com.androidx.resource.dddBug
import kotlinx.android.synthetic.main.activity_viewpager2fragment.*

/**
 * Created by zhangyuncai on 2020/9/1.
 */
class Viewpager2FragmentActivity : AppCompatActivity() {
    private val content = StringBuffer()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_viewpager2fragment)

        val fragmentList = mutableListOf<ViewpagerChildFragment>()
        for (index in 1..6) {
            fragmentList.add(ViewpagerChildFragment().apply {
                this.arguments = Bundle().apply {
                    this.putInt("index", index)
                }
            })
        }
        val viewpager = vp2_viewpager2fragment
        val fragmentAdapter = object : FragmentStateAdapter(
            supportFragmentManager,
            lifecycle
        ) {
            override fun getItemCount(): Int {
                return fragmentList.size
            }

            override fun createFragment(position: Int): Fragment {
                return fragmentList[position]
            }

        }
        viewpager.adapter = fragmentAdapter
        if (false) {//关闭预加载
            if (false) {
                (viewpager.getChildAt(0) as RecyclerView).layoutManager!!.isItemPrefetchEnabled =
                    false
            } else {
                (viewpager.getChildAt(0) as RecyclerView).setItemViewCacheSize(1)
            }
        }

        tv_viewpager2fragment_log.movementMethod = ScrollingMovementMethod()
    }

    fun addText(text: String) {
        content.append("${text}\n")
        tv_viewpager2fragment_log.text = content.toString()
        dddBug(text)
    }
}

/*
//todo 第一次创建
1,onAttach
1,onCreate
1,onCreateView
1,onViewCreated
1,onActivityCreated
1,onViewStateRestored
1,onStart
1,onResume

//todo 滑动下一页
2,onAttach
2,onCreate
2,onCreateView
2,onViewCreated
2,onActivityCreated
2,onViewStateRestored
2,onStart
1,onPause
2,onResume

//todo 进入其他页面并返回
2,onPause
1,onStop
2,onStop
1,onStart
2,onStart
2,onResume
 */
class ViewpagerChildFragment : Fragment() {
    var index: Int = 0
    val mActivity by lazy { requireActivity() as Viewpager2FragmentActivity }
    private fun addText(text: String) {
        mActivity.addText("index:$index,$text")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.let {
            index = it.getInt("index", 0)
        }
        addText("onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addText("onCreate")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val button = Button(requireContext())

        addText("onCreateView")

        button.text = "ViewpagerChildFragment:${index}"
        return button
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addText("onViewCreated")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        addText("onActivityCreated")
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        addText("onViewStateRestored")
    }

    override fun onStart() {
        super.onStart()
        addText("onStart")
    }

    override fun onResume() {
        super.onResume()
        addText("onResume")
    }

    override fun onPause() {
        super.onPause()
        addText("onPause")
    }

    override fun onStop() {
        super.onStop()
        addText("onStop")
    }


    override fun onDestroyView() {
        super.onDestroyView()
        addText("onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        addText("onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        addText("onDetach")
    }


    /**
     * todo show/hide管用
     */
    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        addText("onHiddenChanged")
    }


    /**
     * todo androidx没有这个生命周期,被移植到onPause中去了
     */
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        addText("setUserVisibleHint")
    }

    /**
     * todo 不知作用
     */
    override fun onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu()
        addText("onDestroyOptionsMenu")
    }

    /**
     * todo 不知作用
     */
    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        addText("onAttachFragment")
    }

    /**
     * todo 不知作用
     */
    override fun onInflate(context: Context, attrs: AttributeSet, savedInstanceState: Bundle?) {
        super.onInflate(context, attrs, savedInstanceState)
        addText("onInflate")
    }

}