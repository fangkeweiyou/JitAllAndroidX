package com.androidx.fragment.viewpagerfragment

import android.content.Context
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import com.androidx.fragment.R
import com.androidx.resource.dddBug
import kotlinx.android.synthetic.main.activity_viewpagerfragment.*

/**
 * Created by zhangyuncai on 2020/9/1.
 */
class ViewpagerFragmentActivity : AppCompatActivity() {
    private val content = StringBuffer()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_viewpagerfragment)

        val fragmentList = mutableListOf<ViewpagerChildFragment>()
        for (index in 1..6) {
            fragmentList.add(ViewpagerChildFragment().apply {
                this.arguments = Bundle().apply {
                    this.putInt("index", index)
                }
            })
        }
        val viewpager = vp_viewpagerfragment
        val fragmentAdapter = object : FragmentPagerAdapter(
            supportFragmentManager,
            BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        ) {
            override fun getItem(position: Int): Fragment {
                return fragmentList[position]
            }

            override fun getCount(): Int {
                return fragmentList.size
            }

        }
        viewpager.adapter = fragmentAdapter
        viewpager.offscreenPageLimit=1

        tv_viewpagerfragment_log.movementMethod = ScrollingMovementMethod()
    }

    fun addText(text: String) {
        content.append("${text}\n")
        tv_viewpagerfragment_log.text = content.toString()
        dddBug(text)
    }
}

/*
//todo 第一次创建
1,onAttach
1,onCreate
2,onAttach
2,onCreate
1,onCreateView
1,onViewCreated
1,onActivityCreated
1,onViewStateRestored
1,onStart
2,onCreateView
2,onViewCreated
2,onActivityCreated
2,onViewStateRestored
2,onStart
1,onResume

//todo 滑动下一页
3,onAttach
3,onCreate
3,onCreateView
3,onViewCreated
3,onActivityCreated
3,onViewStateRestored
3,onStart
1,onPause
2,onResume

//todo 滑动下一页,销毁的生命周期
4,onAttach
4,onCreate
1,onStop
1,onDestroyView
4,onCreateView
4,onViewCreated
4,onActivityCreated
4,onViewStateRestored
4,onStart
2,onPause
3,onResume

//todo 进入其他页面并返回
2,onPause
1,onStop
2,onStop
3,onStop
1,onStart
2,onStart
3,onStart
2,onResume
 */
class ViewpagerChildFragment : Fragment() {
    var index: Int = 0
    val mActivity by lazy { requireActivity() as ViewpagerFragmentActivity }
    private fun addText(text: String) {
        mActivity.addText("index:$index,$text")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.let {
            index = it.getInt("index", 0)
        }
        addText("onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addText("onCreate")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val button = Button(requireContext())

        addText("onCreateView")

        button.text = "ViewpagerChildFragment:${index}"
        return button
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addText("onViewCreated")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        addText("onActivityCreated")
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        addText("onViewStateRestored")
    }

    override fun onStart() {
        super.onStart()
        addText("onStart")
    }

    override fun onResume() {
        super.onResume()
        addText("onResume")
    }

    override fun onPause() {
        super.onPause()
        addText("onPause")
    }

    override fun onStop() {
        super.onStop()
        addText("onStop")
    }


    override fun onDestroyView() {
        super.onDestroyView()
        addText("onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        addText("onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        addText("onDetach")
    }


    /**
     * todo show/hide管用
     */
    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        addText("onHiddenChanged")
    }

    /**
     * todo androidx没有这个生命周期
     */
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        addText("setUserVisibleHint")
    }

    /**
     * todo 不知作用
     */
    override fun onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu()
        addText("onDestroyOptionsMenu")
    }

    /**
     * todo 不知作用
     */
    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        addText("onAttachFragment")
    }

    /**
     * todo 不知作用
     */
    override fun onInflate(context: Context, attrs: AttributeSet, savedInstanceState: Bundle?) {
        super.onInflate(context, attrs, savedInstanceState)
        addText("onInflate")
    }

}