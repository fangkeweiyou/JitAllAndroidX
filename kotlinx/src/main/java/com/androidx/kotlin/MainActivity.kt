package com.androidx.kotlin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.androidx.kotlin.`as`.AsTest

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AsTest().test()
    }

}