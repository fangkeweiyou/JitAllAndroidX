package com.androidx.kotlin.`as`

import com.androidx.resource.dddBug

/**
 * Created by zhangyuncai on 2020/8/26.
 */
class AsTest {
    fun test()
    {
        val any:Any?=null
        val b = any as Boolean? ?: true
        dddBug("b:$b")
    }
}