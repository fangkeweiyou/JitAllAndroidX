//package com.androidx.kotlin.reified
//
//import androidx.appcompat.app.AppCompatActivity
//import androidx.lifecycle.ViewModel
//import androidx.lifecycle.ViewModelProvider
//import androidx.lifecycle.ViewModelProviders
//import com.androidx.resource.dddBug
//
///**
// * Created by zhangyuncai on 2020/8/18.
// */
//class ReifildTest {
//    public inline fun <reified T : Any> test() {
//        val clazz = T::class.java
//        val newInstance = clazz.newInstance()
//        dddBug("类型:${newInstance.toString()}")
//    }
//}
//
////todo 第一种写法
//abstract class BaseActivity<VIEWMODEL : ViewModel> : AppCompatActivity() {
//    val viewModel: VIEWMODEL by lazy {
//        createViewModel()
//    }
//
//    abstract fun createViewModel(): VIEWMODEL
//}
//
//class GoodsDetailViewModel : ViewModel() {
//
//}
//
//class GoodsDetailActivity : BaseActivity<GoodsDetailViewModel>() {
//    override fun createViewModel(): GoodsDetailViewModel {
//        return ViewModelProvider(this).get(GoodsDetailViewModel::class.java)
//    }
//}
//
////todo 第二种写法
//abstract class Base2Activity<VIEWMODEL : Class<ViewModel>> : AppCompatActivity() {
//    val viewModel: VIEWMODEL by lazy {
//        ViewModelProvider(this).get(VIEWMODEL)
//    }
//}