package com.androidx.kotlin.泛型;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangyuncai on 2020/8/18.
 */
class JavaTest {

    interface ExtendsClass<T> {
        List<? extends T> test();//todo 子类
    }

    interface SuperClass<T> {
        void test(List<? super T> list);//todo 超类
    }

    public void test() {
        ExtendsClass<PersonBean> extendsClass = new ExtendsClass<PersonBean>() {
            @Override
            public List<? extends PersonBean> test() {
                //todo 只能是Person10的子类包括自己
                List<ChildBean> list = new ArrayList<>();
                return list;
            }
        };

        SuperClass<PersonBean> superClass = new SuperClass<PersonBean>() {
            @Override
            public void test(List<? super PersonBean> list) {
                //todo 只能是Person10的父类包括自己
            }
        };
        List<GrandBean> list = new ArrayList<>();
        superClass.test(list);
    }
}
