package com.androidx.kotlin.泛型

/**
 * Created by zhangyuncai on 2020/8/18.
 */
class KotlinTest {
    fun test() {
        object :KotlinExtendsClass<PersonBean>{
            override fun test(): MutableList<out PersonBean> {
                //todo 只能是Person10的子类包括自己
                val list= mutableListOf<ChildBean>()
                return list
            }

        }
        val superClass = object : KotlinSuperClass<PersonBean> {
            override fun test(list: MutableList<in PersonBean>) {
                //todo 只能是Person10的父类包括自己
            }

        }
        val list= mutableListOf<GrandBean>()
        superClass.test(list)
    }
}

interface KotlinExtendsClass<T> {
    fun test(): MutableList<out T>//todo  协变
}

interface KotlinSuperClass<T> {
    fun test(list: MutableList<in T>)//todo 逆变
}