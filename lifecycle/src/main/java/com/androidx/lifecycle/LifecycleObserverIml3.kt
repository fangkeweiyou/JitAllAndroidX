package com.androidx.lifecycle

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.androidx.resource.dddBug


/**
 * Created by zhangyuncai on 2020/8/13.
 */
class LifecycleObserverIml3: DefaultLifecycleObserver {
    override fun onResume(owner: LifecycleOwner) {
        dddBug("onResume3",this)
    }
}