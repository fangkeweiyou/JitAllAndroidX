package com.androidx.lifecycle

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //可以添加多个对象来监听生命周期
        lifecycle.addObserver(LifecycleObserverIml1())
        lifecycle.addObserver(LifecycleObserverIml2())
        lifecycle.addObserver(LifecycleObserverIml3())
    }
}