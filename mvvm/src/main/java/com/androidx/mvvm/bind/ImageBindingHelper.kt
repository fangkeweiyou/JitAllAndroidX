package com.androidx.mvvm.bind

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.androidx.mvvm.R
import com.bumptech.glide.Glide

/**
 * Created by zhangyuncai on 2019/12/21.
 * @Note   todo databinding辅助类
 */
object ImageBindingHelper {

    /**
     * @Note   加载图片
     * @param  imageView 需要加载图片的ImageView
     * @param  url       图片Url
     */
    @BindingAdapter(value = ["bind:url"], requireAll = false)
    @JvmStatic
    fun loadImage(imageView: ImageView?, url: String?) {
        if (imageView == null || url == null || url == "") {
            return
        }
        val context=imageView.context!!
       Glide.with(imageView!!.context)
           .load(url!!)
           .error(R.drawable.ic_launcher_foreground)
           .into(imageView!!)
    }


}