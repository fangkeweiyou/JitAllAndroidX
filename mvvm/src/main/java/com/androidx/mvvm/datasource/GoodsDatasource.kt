package com.androidx.mvvm.datasource

import android.os.SystemClock
import androidx.lifecycle.MutableLiveData
import com.androidx.mvvm.model.GoodsModel
import kotlin.concurrent.thread

/**
 * Created by zhangyuncai on 2020/8/21.
 */
class GoodsDatasource {
    val goodsLiveData = MutableLiveData<GoodsModel>()

    fun getGoodsDetail() {
        thread {
            SystemClock.sleep(1000)
            goodsLiveData.postValue(GoodsModel().apply {
                goodsName = "韩式短裙"
            })
        }
    }
}