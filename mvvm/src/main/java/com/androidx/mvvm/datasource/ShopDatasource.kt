package com.androidx.mvvm.datasource

import android.os.SystemClock
import androidx.lifecycle.MutableLiveData
import com.androidx.mvvm.model.ShopModel
import kotlin.concurrent.thread

/**
 * Created by zhangyuncai on 2020/8/21.
 */
class ShopDatasource {
    val shopLiveData=MutableLiveData<ShopModel>()

    fun getShopDetail()
    {
        thread {
            SystemClock.sleep(1000)
            shopLiveData.postValue(ShopModel().apply {
                shopName="广州最帅的店铺"
            })
        }
    }
}