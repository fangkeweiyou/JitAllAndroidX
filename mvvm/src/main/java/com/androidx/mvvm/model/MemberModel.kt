package com.androidx.mvvm.model

import java.io.Serializable

/**
 * Created by zhangyuncai on 2020/8/21.
 */
class MemberModel() : Serializable {
    companion object{
        var currentMemberModel:MemberModel?=null
    }
    var name: String? = null
        get() {
            return if (field.isNullOrEmpty()) "未登录" else field
        }
    var age: Int? = 0
    var genger: String? = null
        get() {
            return if (field.isNullOrEmpty()) "未知" else field
        }
    var headUrl: String? = null

}