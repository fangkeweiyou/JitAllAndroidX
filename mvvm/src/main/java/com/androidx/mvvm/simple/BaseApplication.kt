package com.androidx.mvvm.simple

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import androidx.multidex.MultiDexApplication


/**
 * Created by zhangyuncai on 2019/12/21.
 * @Note   Application基类
 */
open class BaseApplication : MultiDexApplication() {

    /**
     * 伴生对象
     */
    companion object {

        /**
         * @Name  appContext
         * @Type  android.support.multidex.MultiDexApplication()
         * @Note  应用Application对象（单例）
         */
        lateinit var sApplication: Application
        lateinit var sContext: Context

    }

    /**
     * @Note   Application创建时调用
     * @param  ...
     */
    override fun onCreate() {
        super.onCreate()
        /**初始化Application对象*/
        sApplication = this
        sContext = this

        /**开始Activity生命周期监听，并启动AutoClearHelper*/
        registerActivityLifecycleCallbacks(object :ActivityLifecycleCallbacks{
            override fun onActivityPaused(activity: Activity) {

            }

            override fun onActivityStarted(activity: Activity) {

            }

            override fun onActivityDestroyed(activity: Activity) {

            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {

            }

            override fun onActivityStopped(activity: Activity) {

            }

            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
            }

            override fun onActivityResumed(activity: Activity) {
            }

        })

    }

}