package com.androidx.mvvm.simple

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.annotation.UiThread
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

/**
 * Created by zhangyuncai on 2020/8/19.
 */
abstract class SimpleActivity<VIEWMODEL : SimpleViewModel, DATABINDING : ViewDataBinding> : AppCompatActivity() {
    val mActivity by lazy { this }
    abstract val viewModelClazz: Class<VIEWMODEL>

    //状态栏文字是否暗色
    open var sIsDark = false
    lateinit var mViewModel: VIEWMODEL
    lateinit var mDataBinding: DATABINDING

    abstract fun getDataBinding(): DATABINDING


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusTrans(sIsDark)
        mDataBinding = getDataBinding()
        setContentView(mDataBinding.root)
        mViewModel = ViewModelProvider(this).get(viewModelClazz)
        mViewModel.loadingLiveData.observe(this, Observer {
            when (it) {
                0 -> {
                    showLoadingBar("")
                }
                1 -> {
                    dismissLoadingBar()
                }
                else -> {
                    showLoadingFailureError()
                }
            }
        })

        initView()
        initEvent()
        initData()
    }

    protected abstract fun initView()
    protected abstract fun initEvent()
    protected abstract fun initData()

    @UiThread
    fun showLoadingBar(content: String) {
    }

    @UiThread
    fun dismissLoadingBar() {
    }

    @UiThread
    fun showLoadingFailureError(throwable: Throwable? = null) {
        dismissLoadingBar()
        throwable?.printStackTrace()
    }

    /**
     * 点击返回按钮
     *
     * @param v
     */
    open fun backClick(v: View) {
        finish()
    }


    /**
     * 设置状态栏的字体颜色是否高亮
     * @param dark true:会显示灰色 : false :会显示白色（亮色）
     */
    open fun setAndroidNativeLightStatusBar(dark: Boolean) {
        val decor = window.decorView
        if (dark) {
            decor.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        } else {
            decor.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        }
    }

    /**
     * 设置全透明状态栏
     * @param isDark 状态栏文字灰色
     */
    open fun setStatusTrans(isDark: Boolean = false) {
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS or WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
        if (isDark) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        } else {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        }
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = Color.TRANSPARENT
    }
}