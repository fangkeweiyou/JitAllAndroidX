package com.androidx.mvvm.simple

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.androidx.resource.dddBug

/**
 * Created by zhangyuncai on 2020/8/19.
 */
abstract class SimpleFragment<VIEWMODEL : SimpleViewModel, DATABINDING : ViewDataBinding> : Fragment() {
    abstract val viewModelClazz: Class<VIEWMODEL>
    lateinit var mViewModel: VIEWMODEL
    lateinit var mDataBinding: DATABINDING
    abstract fun getDataBinding(container: ViewGroup?): DATABINDING

    protected val mActivity: SimpleActivity<*, *> by lazy {
        requireActivity() as SimpleActivity<*, *>
    }

    lateinit var rootView: View

    var hasFetchData = false// 标识已经触发过懒加载数据

    init {
        dddBug("init",this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dddBug("onCreate",this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dddBug("onCreateView1",this)
        if (this::rootView.isInitialized) {
            return rootView
        }
        mDataBinding = getDataBinding(container)
        mViewModel = ViewModelProvider(this).get(viewModelClazz)
        mViewModel.loadingLiveData.observe(SimpleFragment@this, Observer {
            when (it) {
                0 -> {
                    showLoadingBar("")
                }
                1 -> {
                    dismissLoadingBar()
                }
                else -> {
                    showLoadingFailureError()
                }
            }
        })
        rootView = mDataBinding.root
        dddBug("onCreateView2",this)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dddBug("onViewCreated",this)
        initView()
        initEvent()
    }

    abstract fun initView()
    abstract fun initEvent()
    abstract fun lazyFetchData()

    fun showLoadingBar(content: String) {
        mActivity.showLoadingBar(content)
    }

    fun dismissLoadingBar() {
        mActivity.dismissLoadingBar()
    }

    fun showLoadingFailureError(throwable: Throwable? = null) {
        mActivity.showLoadingFailureError(throwable)
    }


    override fun onResume() {
        super.onResume()
        lazyFetchDataIfPrepared()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        lazyFetchDataIfPrepared()
    }

    private fun lazyFetchDataIfPrepared() {
        // 用户可见fragment && 没有加载过数据 && 视图已经准备完毕
        if ((this::rootView.isInitialized) && !hasFetchData && !isHidden) {
            hasFetchData = true
            lazyFetchData()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        hasFetchData = false
    }

}