package com.androidx.mvvm.simple

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by zhangyuncai on 2020/8/19.
 */
abstract class SimpleViewModel : ViewModel() {
    val application by lazy {
        BaseApplication.sApplication
    }
    val context by lazy {
        BaseApplication.sContext
    }
    val compositeDisposable = CompositeDisposable()
    var list = mutableListOf<String>()
    var loadingLiveData = MutableLiveData<Int>()


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}