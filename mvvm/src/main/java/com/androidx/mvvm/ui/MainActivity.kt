package com.androidx.mvvm.ui

import android.os.Bundle
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.androidx.mvvm.R
import com.androidx.mvvm.databinding.ActivityMainBinding
import com.androidx.mvvm.model.MemberModel
import com.androidx.mvvm.simple.SimpleActivity
import com.androidx.mvvm.ui.fragment.HomeFragmentDirections
import com.androidx.mvvm.viewmodel.MainViewModel


class MainActivity() : SimpleActivity<MainViewModel, ActivityMainBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override val viewModelClazz: Class<MainViewModel>
        get() = MainViewModel::class.java

    override fun getDataBinding(): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }

    override fun initView() {
    }

    override fun initEvent() {
        val findNavController = Navigation.findNavController(this, R.id.nav_main)
        //第一种方式绑定bottomNavigationView和NavHostFragment
//        mDataBinding.bottomNavigationView.setupWithNavController(findNavController)
        mDataBinding.bottomNavigationView.setOnNavigationItemSelectedListener {
            if(it.itemId== R.id.meFragment) {
                if (MemberModel.currentMemberModel == null) {
                    val action =
                        HomeFragmentDirections.actionHomeFragmentToLoginFragment()
                    mActivity.findNavController(R.id.nav_main).navigate(action)
                    return@setOnNavigationItemSelectedListener false
                }
            }
            //第二种方式绑定bottomNavigationView和NavHostFragment
            NavigationUI.onNavDestinationSelected(it,findNavController)
            return@setOnNavigationItemSelectedListener true
        }

    }

    override fun initData() {
    }

}