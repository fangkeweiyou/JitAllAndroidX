package com.androidx.mvvm.ui

import androidx.lifecycle.Observer
import com.androidx.mvvm.databinding.ActivityShopBinding
import com.androidx.mvvm.simple.SimpleActivity
import com.androidx.mvvm.viewmodel.ShopViewModel

/**
 * Created by zhangyuncai on 2020/8/21.
 */
class ShopActivity:SimpleActivity<ShopViewModel,ActivityShopBinding>() {
    override val viewModelClazz: Class<ShopViewModel>
        get() = ShopViewModel::class.java

    override fun getDataBinding(): ActivityShopBinding {
        return ActivityShopBinding.inflate(layoutInflater)
    }

    override fun initView() {
    }

    override fun initEvent() {
    }

    override fun initData() {
        mViewModel.getShopDetail()
        mViewModel.shopRepository.shopDatasource.shopLiveData.observe(this, Observer {
            mDataBinding.shopModel=it
        })
        mViewModel.getGoodsDetail()
        mViewModel.shopRepository.goodsDatasource.goodsLiveData.observe(this, Observer {
            mDataBinding.goodsModel=it
        })
    }

}