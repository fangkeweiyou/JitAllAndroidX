package com.androidx.mvvm.ui.fragment

import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebViewClient
import com.androidx.mvvm.databinding.FragmentHomeBinding
import com.androidx.mvvm.simple.SimpleFragment
import com.androidx.mvvm.viewmodel.HomeViewModel

/**
 * Created by zhangyuncai on 2020/8/21.
 */
class HomeFragment : SimpleFragment<HomeViewModel, FragmentHomeBinding>() {
    override val viewModelClazz: Class<HomeViewModel>
        get() = HomeViewModel::class.java

    override fun getDataBinding(container: ViewGroup?): FragmentHomeBinding {
        return FragmentHomeBinding.inflate(layoutInflater, container, false)
    }

    override fun initView() {

    }

    override fun initEvent() {
    }

    override fun lazyFetchData() {
        mDataBinding.webHome.run {
            webViewClient = object : WebViewClient() {}
            webChromeClient = object:WebChromeClient(){}
            loadUrl("http://www.fangkeweiyou.com")
        }
    }
}