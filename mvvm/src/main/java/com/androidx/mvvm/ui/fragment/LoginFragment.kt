package com.androidx.mvvm.ui.fragment

import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.androidx.mvvm.databinding.FragmentLoginBinding
import com.androidx.mvvm.model.MemberModel.Companion.currentMemberModel
import com.androidx.mvvm.simple.SimpleFragment
import com.androidx.mvvm.viewmodel.LoginViewModel
import com.androidx.resource.showToast

/**
 * Created by zhangyuncai on 2020/8/21.
 */
class LoginFragment:SimpleFragment<LoginViewModel,FragmentLoginBinding>() {
    override val viewModelClazz: Class<LoginViewModel>
        get() = LoginViewModel::class.java

    override fun getDataBinding(container: ViewGroup?): FragmentLoginBinding {
        return FragmentLoginBinding.inflate(layoutInflater,container,false)
    }

    override fun initView() {
        mDataBinding.viewmodel=mViewModel
        mViewModel.loginStateLiveData.observe(this, Observer {
            it?.let {
                currentMemberModel=it
                showToast("登录成功")
                //关闭当前界面
                findNavController().navigateUp()
            }
        })
    }

    override fun initEvent() {

    }

    override fun lazyFetchData() {

    }
}