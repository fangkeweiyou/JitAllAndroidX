package com.androidx.mvvm.ui.fragment

import android.content.Intent
import android.view.ViewGroup
import com.androidx.mvvm.databinding.FragmentMeBinding
import com.androidx.mvvm.model.MemberModel.Companion.currentMemberModel
import com.androidx.mvvm.simple.SimpleFragment
import com.androidx.mvvm.ui.ShopActivity
import com.androidx.mvvm.viewmodel.MeViewModel

/**
 * Created by zhangyuncai on 2020/8/21.
 */
class MeFragment: SimpleFragment<MeViewModel,FragmentMeBinding>(){
    override val viewModelClazz: Class<MeViewModel>
        get() = MeViewModel::class.java

    override fun getDataBinding(container: ViewGroup?): FragmentMeBinding {
        return FragmentMeBinding.inflate(layoutInflater,container,false)
    }

    override fun initView() {
        currentMemberModel?.let {
            mDataBinding.model=it
        }
    }

    override fun initEvent() {
        mDataBinding.btMeShop.setOnClickListener {
            startActivity(Intent(mActivity,ShopActivity::class.java))
        }
    }

    override fun lazyFetchData() {

    }
}