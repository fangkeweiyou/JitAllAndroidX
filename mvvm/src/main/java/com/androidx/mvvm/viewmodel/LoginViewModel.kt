package com.androidx.mvvm.viewmodel

import android.os.SystemClock
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.androidx.mvvm.model.MemberModel
import com.androidx.mvvm.simple.SimpleViewModel
import com.androidx.resource.dddBug
import kotlin.concurrent.thread

/**
 * Created by zhangyuncai on 2020/8/21.
 */
class LoginViewModel : SimpleViewModel() {
    val usernameOb = ObservableField<String>()
    val passwordOb = ObservableField<String>()
    val loginStateLiveData = MutableLiveData<MemberModel>()

    fun login() {
        thread {
            dddBug("账号:${usernameOb.get()},密码:${passwordOb.get()}")
            dddBug("正在登录...")
            SystemClock.sleep(1000)
            val memberModel = MemberModel().apply {
                name = "牛逼"
                genger = "女"
                age = 90
                headUrl = "http://www.fangkeweiyou.com/meinv/1"
            }
            loginStateLiveData.postValue(memberModel)
        }
    }
}