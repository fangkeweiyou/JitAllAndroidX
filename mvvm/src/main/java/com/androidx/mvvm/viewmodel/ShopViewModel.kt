package com.androidx.mvvm.viewmodel

import com.androidx.mvvm.repository.ShopRepository
import com.androidx.mvvm.simple.SimpleViewModel

/**
 * Created by zhangyuncai on 2020/8/21.
 */
class ShopViewModel : SimpleViewModel() {
    val shopRepository = ShopRepository()

    fun getShopDetail() {
        shopRepository.shopDatasource.getShopDetail()
    }

    fun getGoodsDetail() {
        shopRepository.goodsDatasource.getGoodsDetail()
    }
}