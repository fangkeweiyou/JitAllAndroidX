package com.androidx.mvvm.viewpager2

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.androidx.mvvm.databinding.ActivityViewpager2Binding
import com.androidx.mvvm.simple.SimpleActivity

/**
 * Created by zhangyuncai on 2020/8/21.
 */
class ViewPager2Activity :
    SimpleActivity<ViewPager2ActivityViewModel, ActivityViewpager2Binding>() {
    override val viewModelClazz: Class<ViewPager2ActivityViewModel>
        get() = ViewPager2ActivityViewModel::class.java

    override fun getDataBinding(): ActivityViewpager2Binding {
        return ActivityViewpager2Binding.inflate(layoutInflater)
    }

    override fun initView() {
        val fragments = mutableListOf<ViewPagerFragment>()
        for (index in 0..3) {
            fragments.add(ViewPagerFragment(index))
        }
        val fragmentAdapter = object : FragmentStateAdapter(this) {
            override fun getItemCount(): Int {
                return fragments.size
            }

            override fun createFragment(position: Int): Fragment {
                return fragments.get(position)
            }

        }
        mDataBinding.vp2Viewpager2.adapter = fragmentAdapter
    }

    override fun initEvent() {
    }

    override fun initData() {
    }

}