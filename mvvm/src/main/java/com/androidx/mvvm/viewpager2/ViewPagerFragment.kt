package com.androidx.mvvm.viewpager2

import android.view.ViewGroup
import com.androidx.mvvm.databinding.FragmentViewpagerBinding
import com.androidx.mvvm.simple.SimpleFragment

/**
 * Created by zhangyuncai on 2020/8/21.
 */
class ViewPagerFragment(var index:Int=0):SimpleFragment<ViewPager2FragmentViewModel,FragmentViewpagerBinding>() {
    override val viewModelClazz: Class<ViewPager2FragmentViewModel>
        get() = ViewPager2FragmentViewModel::class.java

    override fun getDataBinding(container: ViewGroup?): FragmentViewpagerBinding {
        return FragmentViewpagerBinding.inflate(layoutInflater,container,false)
    }

    override fun initView() {
        mDataBinding.tvViewpagerContent.text="${index}"
    }

    override fun initEvent() {
    }

    override fun lazyFetchData() {
    }
}