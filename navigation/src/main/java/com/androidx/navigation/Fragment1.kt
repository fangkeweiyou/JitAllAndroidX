package com.androidx.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.androidx.resource.BaseFragment
import com.androidx.resource.dddBug
import kotlinx.android.synthetic.main.fragment_1.*

/**
 * Created by zhangyuncai on 2020/8/5.
 */
class Fragment1: BaseFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dddBug("onCreate",this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dddBug("onCreateView",this)
        return inflater.inflate(R.layout.fragment_1,container,false)
    }

    var index=0
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button1.setOnClickListener {view->
            val action = Fragment1Directions.actionFragment1ToFragment2(101,"流行女装")
            Toast.makeText(requireActivity(),"button1",Toast.LENGTH_SHORT).show()
            view.findNavController().navigate(action)
            button1.text="已经跳往Fragment2,第几次${index++}"
        }
        button2.setOnClickListener {
            val action = Fragment1Directions.actionFragment1ToFragment3()
            view.findNavController().navigate(action)
        }
        button3.setOnClickListener {
            val action = Fragment1Directions.actionFragment1ToFragment4()
            view.findNavController().navigate(action)
        }
        requireActivity()

    }
}