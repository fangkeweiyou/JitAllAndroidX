package com.androidx.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.androidx.resource.BaseFragment

/**
 * Created by zhangyuncai on 2020/8/5.
 */
class Fragment2: BaseFragment() {

    val args by lazy { Fragment2Args.fromBundle(requireArguments()) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_2,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        System.out.println("---<<<>>>---传递参数:${args.goodsId},商品名:${args.goodsName}");

    }

}