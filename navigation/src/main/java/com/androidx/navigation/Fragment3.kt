package com.androidx.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.androidx.resource.BaseFragment
import kotlinx.android.synthetic.main.fragment_1.*

/**
 * Created by zhangyuncai on 2020/8/5.
 */
class Fragment3: BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_3,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button1.setOnClickListener {
            val action = Fragment3Directions.actionFragment3ToFragment5()
            it.findNavController().navigate(action)
        }
    }

}