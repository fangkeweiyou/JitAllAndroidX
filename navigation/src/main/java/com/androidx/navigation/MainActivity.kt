package com.androidx.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavHostController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.androidx.resource.dddBug
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragment = fragment as NavHostFragment
        val findNavController = fragment.findNavController() as NavHostController
        findNavController.enableOnBackPressed(true)
        dddBug("findNavController:${findNavController==null}",this)
    }
}