package com.androidx.navigation.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import com.androidx.navigation.R

/**
 * Created by zhangyuncai on 2020/8/25.
 */
class MultiNavigationActivity:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multinavigation)

        if(false) {
            val navHostFragment =
                supportFragmentManager.findFragmentById(R.id.host_multinavigation) as NavHostFragment
            navHostFragment.navController.setGraph(R.navigation.nav_graph)
        }else if(false){
            val navHostFragment =
                supportFragmentManager.findFragmentById(R.id.host_multinavigation) as NavHostFragment
            navHostFragment.navController.setGraph(R.navigation.nav2)
        }else if(true){
            val navHostFragment =
                supportFragmentManager.findFragmentById(R.id.host_multinavigation) as NavHostFragment
            navHostFragment.navController.setGraph(R.navigation.nav3)
        }
    }
}