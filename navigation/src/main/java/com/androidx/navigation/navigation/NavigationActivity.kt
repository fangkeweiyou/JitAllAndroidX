package com.androidx.navigation.navigation

import android.os.Bundle
import com.androidx.navigation.R
import com.androidx.resource.BaseActivity

/**
 * Created by zhangyuncai on 2020/8/13.
 */
class NavigationActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)

        /*defaultNavHost false todo 表示结束当前页面*/
    }
}