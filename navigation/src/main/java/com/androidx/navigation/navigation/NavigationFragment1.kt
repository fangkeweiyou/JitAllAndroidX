package com.androidx.navigation.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.androidx.navigation.R
import kotlinx.android.synthetic.main.fragment_navigationfragment1.*

/**
 * Created by zhangyuncai on 2020/8/13.
 */
class NavigationFragment1: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_navigationfragment1,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bt_fragment1_skip.setOnClickListener {
            val actionNavigationFragment1ToNavigationFragment2 =
                NavigationFragment1Directions.actionNavigationFragment1ToNavigationFragment2()
            view.findNavController().navigate(actionNavigationFragment1ToNavigationFragment2)
        }
    }
}