package com.androidx.paging

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.androidx.paging.model.GoodsModel
import com.androidx.resource.dddBug

/**
 * Created by zhangyuncai on 2020/8/5.
 */
class GoodsAdapter() :
    PagedListAdapter<GoodsModel, GoodsAdapter.GoodsHolder>(DIFF_CALLBACK) {

    companion object{
        /*
         //对数据源返回的数据进行了比较处理,
        //它的意义是——我需要知道怎么样的比较，
        //才意味着数据源的变化，并根据变化再进行的UI刷新操作
         */
        private val DIFF_CALLBACK=object :DiffUtil.ItemCallback<GoodsModel>(){
            override fun areItemsTheSame(oldItem: GoodsModel, newItem: GoodsModel): Boolean {
                dddBug("areItemsTheSame:${oldItem.id}/${newItem.id}:${oldItem.id==newItem.id}")
//                System.out.println("---<<<>>>---areItemsTheSame");
                return oldItem.id==newItem.id
//                return false
            }

            override fun areContentsTheSame(oldItem: GoodsModel, newItem: GoodsModel): Boolean {
                dddBug("areItemsTheSame${oldItem.id}/${newItem.id}:${oldItem==newItem}")
//                System.out.println("---<<<>>>---areContentsTheSame")
                return oldItem==newItem
//                return false
            }


        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GoodsHolder {
        val dataBinding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_goods_adapter,
            parent,
            false
        )
        return GoodsHolder(dataBinding)
    }

    override fun onBindViewHolder(holder: GoodsHolder, position: Int) {
        holder.dataBinding.setVariable(BR.goodsModel,getItem(position))
//        System.out.println("---<<<>>>---onBindViewHolder");
    }

    inner class GoodsHolder(var dataBinding:ViewDataBinding) : RecyclerView.ViewHolder(dataBinding.root) {

    }

}
