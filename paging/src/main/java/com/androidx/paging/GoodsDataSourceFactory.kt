package com.androidx.paging

import androidx.paging.DataSource
import com.androidx.paging.datasource.GoodsPositionalDataSource
import com.androidx.paging.model.GoodsModel

/**
 * Created by zhangyuncai on 2020/8/6.
 */
class GoodsDataSourceFactory : DataSource.Factory<Int, GoodsModel>() {
    override fun create(): DataSource<Int, GoodsModel> {
        //PositionalDataSource：按位置加载，如加载指定从第n条到n+20条
        return GoodsPositionalDataSource()
        //PageKeyedDataSource：按页加载，如请求数据时传入page页码
//        return GoodsPageDataSource()
        //ItemKeyedDataSource：按条目加载，即请求数据需要传入其它item的信 息，如加载第n+1项的数据需传入第n项的id
//        return GoodsItemDataSource()
    }
}