package com.androidx.paging

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.androidx.paging.model.GoodsModel

/**
 * Created by zhangyuncai on 2020/8/6.
 */
class GoodsViewModel : ViewModel() {
    var goods: LiveData<PagedList<GoodsModel>>

    init {
        //初始化数据工厂
        val factory = GoodsDataSourceFactory()
        //初始化分页配置
        val config = PagedList.Config.Builder().apply {
            setPageSize(20) //每页显示条目数量
            setInitialLoadSizeHint(20)//首次加载条目数量 默认为 pageSize * 3
            setEnablePlaceholders(false)//当item为null是否使用PlaceHolder展示
            setPrefetchDistance(1)//距离底部多少条数据开始预加载，设置0则表示滑到底部才加载
        }.build()
        goods = LivePagedListBuilder<Int, GoodsModel>(factory, config).build()
    }

}