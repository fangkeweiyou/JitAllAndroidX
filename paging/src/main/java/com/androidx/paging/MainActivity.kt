package com.androidx.paging

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.androidx.paging.databinding.ActivityMainBinding
import com.androidx.resource.BaseActivity
//https://www.jianshu.com/p/03054d577d8d
class MainActivity : BaseActivity() {
    val mAdapter by lazy { GoodsAdapter() }
    lateinit var goodsViewModel:GoodsViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)

        val binding= DataBindingUtil.setContentView<ActivityMainBinding>(this,R.layout.activity_main)

        goodsViewModel=ViewModelProvider(this,ViewModelProvider.NewInstanceFactory()).get(GoodsViewModel::class.java)
        binding.rvPagingMain.adapter=mAdapter
        goodsViewModel.goods.observe(this, Observer {
//            mAdapter.currentList?.getOrNull(8)
            mAdapter.submitList(it)
        })
        binding.swipeMain.setOnRefreshListener {
            binding.swipeMain.isRefreshing=false
            goodsViewModel.goods.value?.dataSource?.invalidate()
        }
        binding.change.setOnClickListener {
            mAdapter.currentList?.getOrNull(8)?.name="我修改了"
            mAdapter.notifyItemChanged(8)
        }
    }

}