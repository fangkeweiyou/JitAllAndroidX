package com.androidx.paging.diffutil

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.androidx.paging.model.GoodsModel
import com.androidx.paging.R
import com.androidx.resource.dddBug

/**
 * Created by zhangyuncai on 2020/8/12.
 */
class DiffAdapter(var list: MutableList<GoodsModel>) :
    RecyclerView.Adapter<DiffAdapter.DiffViewHolder>() {
    inner class DiffViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        val tv_diff_content: TextView

        init {
            tv_diff_content = view.findViewById(R.id.tv_diff_content)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiffViewHolder {
        return DiffViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_diff_adapter, parent,false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: DiffViewHolder, position: Int) {
        dddBug("onBindViewHolder:${position}")
        holder.tv_diff_content.setText(list[position].name)
    }

}

