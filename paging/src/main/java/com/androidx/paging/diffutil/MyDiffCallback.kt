package com.androidx.paging.diffutil

import androidx.recyclerview.widget.DiffUtil
import com.androidx.paging.model.GoodsModel
import com.androidx.resource.dddBug

/**
 * Created by zhangyuncai on 2020/8/12.
 */
class MyDiffCallback(
    var oldList: MutableList<GoodsModel>,
    var newList: MutableList<GoodsModel>
) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem=oldList.get(oldItemPosition)
        val newItem=newList.get(newItemPosition)
        dddBug("areItemsTheSame:${oldItemPosition}/${newItemPosition}:${oldItem.id}/${newItem.id}:${oldItem.id==newItem.id}")
        return oldItem.id==newItem.id
    }

    override fun getOldListSize(): Int {
        dddBug("getOldListSize")
        return oldList.size
    }

    override fun getNewListSize(): Int {
        dddBug("getNewListSize")
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem=oldList.get(oldItemPosition)
        val newItem=newList.get(newItemPosition)
        dddBug("areItemsTheSame:${oldItemPosition}/${newItemPosition}:${oldItem.name}/${newItem.name}:${oldItem==newItem}")
        return  oldItem==newItem
    }

}