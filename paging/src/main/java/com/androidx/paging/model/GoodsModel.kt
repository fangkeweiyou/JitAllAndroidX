package com.androidx.paging.model

/**
 * Created by zhangyuncai on 2020/8/5.
 */
data  class GoodsModel constructor(
    var id:Int,
    var name:String,
    var img:String
){
    constructor():this(0,"","")

    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }
}