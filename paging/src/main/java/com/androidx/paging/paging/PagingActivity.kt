package com.androidx.paging.paging

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.androidx.paging.GoodsAdapter
import com.androidx.paging.R
import com.androidx.paging.databinding.ActivityPagingBinding
import com.androidx.resource.BaseActivity

/**
 * Created by zhangyuncai on 2020/8/13.
 */
class PagingActivity:BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding =
            DataBindingUtil.setContentView<ActivityPagingBinding>(this, R.layout.activity_paging)
        val goodsAdapter=GoodsAdapter()
    }
}