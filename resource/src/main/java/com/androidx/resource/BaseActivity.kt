package com.androidx.resource

import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by zhangyuncai on 2020/8/4.
 */
open class BaseActivity : AppCompatActivity() {
    protected val mActivity: AppCompatActivity by lazy { this }

    protected fun newButton(content: String, onClickListener: View.OnClickListener): Button {
        val button = Button(mActivity)
        button.text = content
        button.setOnClickListener(onClickListener)
        return button
    }
}