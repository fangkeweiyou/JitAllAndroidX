package com.androidx.resource

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

/**
 * Created by zhangyuncai on 2020/8/8.
 */
open class BaseFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dddBug("onCreateView", this)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dddBug("onViewCreated", this)
    }

    override fun onStart() {
        super.onStart()
        dddBug("onStart", this)
    }


    override fun onResume() {
        super.onResume()
        dddBug("onResume", this)
    }

    override fun onPause() {
        super.onPause()
        dddBug("onPause", this)
    }

    override fun onStop() {
        super.onStop()
        dddBug("onStop", this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        dddBug("onDestroyView", this)
    }

}