package com.androidx.resource

import android.content.Context
import android.widget.Toast
import timber.log.Timber

/**
 * Created by zhangyuncai on 2020/8/5.
 */

lateinit var sContext: Context
//lateinit var sApplication:Application

fun dddBug(content: Any, clazz: Class<Any>? = null) {
    if (clazz != null) {
        Timber.d("${clazz.simpleName}--->>>>>>>>---:${content}")
    } else {
        Timber.d("--->>>>>>>>---:${content}")
    }
}

fun dddBug(content: Any, obj: Any? = null) {
    if (obj != null) {
        Timber.d("${obj::class.java.simpleName}--->>>>>>>>---:${content}")
    } else {
        Timber.d("--->>>>>>>>---:${content}")
    }
}

fun dddBug(content: Any) {
    Timber.d("--->>>>>>>>---:${content}")
}

fun showToast(content: String) {
    Toast.makeText(sContext, content, Toast.LENGTH_SHORT).show()
}