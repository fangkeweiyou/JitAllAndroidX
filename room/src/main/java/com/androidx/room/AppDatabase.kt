package com.androidx.room

import androidx.room.Database
import androidx.room.RoomDatabase

/**
 * Created by zhangyuncai on 2020/8/5.
 */
@Database(entities = arrayOf(User::class),version = 3)
abstract class AppDatabase:RoomDatabase() {
    abstract fun userDao():UserDao
}