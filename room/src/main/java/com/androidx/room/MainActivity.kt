package com.androidx.room

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.androidx.resource.dddBug
import com.androidx.room.RoomApplication.Companion.sDb
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getAll.setOnClickListener {
            thread {
                sDb.userDao().getAll().forEach {
                    dddBug("user:${it}")
                }
            }

        }
        inser.setOnClickListener {
            thread {
                sDb.userDao().insertAll(User().apply {
                    uid=11
                    firstName="zhang"
                    lastName="san"
                    gender="fale"
                })
            }

        }
    }

}