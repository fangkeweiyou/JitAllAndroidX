package com.androidx.room

import android.app.Application
import androidx.room.Room

/**
 * Created by zhangyuncai on 2020/8/5.
 */
class RoomApplication:Application() {
    companion object{
        lateinit var sDb:AppDatabase
    }

    override fun onCreate() {
        sDb= Room.databaseBuilder(this, AppDatabase::class.java, "room")
            .fallbackToDestructiveMigration()//如果升级数据库,就清空原有的,创建新的
            .build()
        super.onCreate()
    }
}