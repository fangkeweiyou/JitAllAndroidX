package com.androidx.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by zhangyuncai on 2020/8/5.
 */
@Entity
class User {
    @PrimaryKey
    var uid:Int=0//这个不允许重复
    @ColumnInfo(name = "first_name")
    var firstName:String?=null
    @ColumnInfo(name = "last_name")
    var lastName:String?=null
    @ColumnInfo(name = "gender")
    var gender:String?=null
    override fun toString(): String {
        return "User(uid=$uid, firstName=$firstName, lastName=$lastName, gender=$gender)"
    }


}