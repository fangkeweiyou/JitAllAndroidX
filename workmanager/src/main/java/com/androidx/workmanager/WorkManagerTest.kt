package com.androidx.workmanager

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.androidx.resource.dddBug

/**
 * Created by zhangyuncai on 2020/8/6.
 */
class WorkManagerTest constructor(context:Context, workerParams:WorkerParameters):Worker(context,workerParams) {
    override fun doWork(): Result {
        dddBug("doWork")
        return Result.success()
    }
}