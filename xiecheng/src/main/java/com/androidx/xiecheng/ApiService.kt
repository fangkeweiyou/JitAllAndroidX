package com.androidx.xiecheng

import io.reactivex.Observable
import io.reactivex.Observer
import okhttp3.ResponseBody
import retrofit2.http.GET

/**
 * Created by zhangyuncai on 2020/8/6.
 */
interface ApiService {
    @GET("/getAppList")
    suspend fun getAppList():ResponseBody
}